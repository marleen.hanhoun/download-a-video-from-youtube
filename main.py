from tkinter import *
from tkinter import filedialog 
from pytube import YouTube 
import threading 


root = Tk()
root.title("youtube download")
root.geometry("600x320")
root.resizable(False,False)


#my function

def Browse():
    directory = filedialog.askdirectory(title="save voice")
    folderlink.delete(0,"end")
    folderlink.insert(0,directory)

def down_yt():
    status.config(text="status :downloading...")
    link = ytlink.get()
    folder = folderlink.get()
    YouTube(link ,on_complete_callback=finsh).streams.filter(progressive=True, file_extension="mp4").order_by("resolution").desc().first().download(folder)

def finsh(stream=None,chunk=None,file_handle=None, remaining=None):
    status.config(text="statuse: complete")


#youtube logo

ytlogo = PhotoImage(file="D:\music\\4.png").subsample(10)
ytTitle = Label(root, image=ytlogo)
ytTitle.place(relx=0.5 , rely=0.25 ,anchor="center")


#youtube link
ytlabel = Label(root, text="Youtube link", bg='#ff6060')  
ytlabel.place(x=25 , y=150)

ytlink = Entry(root, width=60)
ytlink.place(x=140 ,y=150)

#download filder
folderlabel = Label(root ,text="Download folder",bg='#ff6060')
folderlabel.place(x=25 ,y=183)

folderlink = Entry(root ,width=50)
folderlink.place(x=140,y=183)

#Browse Button
browse = Button(root ,text ="browser",command=Browse,bg='#ff6060',borderwidth='5')
browse.place(x=455 ,y=180)

#download Button
download = Button(root ,text ="dowload",command=threading.Thread(target=down_yt).start,bg='#ff6060',borderwidth='5')
download.place(x=280, y=220)

#statuse bar
status = Label(root,text="status: Ready", font="Calibre 10 italic",fg="black",bg="white", anchor="w")
status.place(rely=1,anchor="sw",relwidth=1)





root.mainloop()